package t92;

public class Veiculo {
    private int tipo;

    
    private String placa;
    public void setTipo(int tipo) { this.tipo = tipo; }
    public void setPlaca(String placa) { this.placa = placa; }
    public String getPlaca(){ return placa; }
    
    public String TipoToString(){
         if( tipo == 0 ) return "Carro";
        else return "Motocicleta";
    }
    public Veiculo(){
        tipo = 0;
        placa = "AAA1234";
    }
    public Veiculo(int _tipo, String _placa){
        tipo = _tipo;
        placa = _placa;
    }
    
    public float GetCustoHora(){
        if( tipo == 0 ) return 3;
        else return (float) 1.50;
    }
            
}
