package t92;
import java.util.Random;

public class Simulador {
    private Estacionamento estac;
    static int hora = 0;
    Veiculo testV;
    Random rand;
    
    public Simulador(){
        rand =  new Random();
        testV = new Veiculo();
    }
    public Simulador(Estacionamento _estac ){
        estac = _estac;
        rand =  new Random();
        testV = new Veiculo();
    }
    
    private void SwapVeiculo()
    {
        testV.setTipo( rand.nextInt( 2 ) );
        testV.setPlaca("AAA"+rand.nextInt( 10000 ));
    }
    public void Simular()
    {
        hora = 0;
        while( hora < 100){
            if( !estac.IsOcupied() ){
                if( rand.nextInt( 100 ) < 30){
                    SwapVeiculo();
                    estac.CarEntry(testV);
                }
            }
            else{
                if( rand.nextInt( 100 ) < 30){
                    estac.CarExit(testV);
                } 
            }
            
            hora++;
        }
    }
}
