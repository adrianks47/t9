package t92;

public class Estacionamento {
    private Veiculo ocupante;
    private int entryTime;
    
    public Estacionamento(){}
    
    public boolean IsOcupied(){
        return (ocupante != null);
    }
    public int DuracaoDeEstada( ){
         return Simulador.hora - entryTime;
    }
    public float CalculatePrice( ){
        return ocupante.GetCustoHora() * DuracaoDeEstada();
    }
    public void CarEntry( Veiculo v )
    {
        ocupante = v;
        entryTime = Simulador.hora;
        System.out.printf( "Hora %d > ", Simulador.hora );
        System.out.printf( "Entrada de %s , Placa = %s\n", v.TipoToString(), v.getPlaca() );
    }
    public void CarExit( Veiculo v )
    {
         System.out.printf( "Hora %d > ", Simulador.hora );
         System.out.printf( "Cobranca na saida = %.2f $, Duracao = %d Hora(s)\n\n", CalculatePrice( ), DuracaoDeEstada( ) );
         ocupante = null;
         
    }
}
