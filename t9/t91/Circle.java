package t91;

public class Circle {
    private final Ponto ponto;
    private float ray;

    public Ponto getPonto() {return ponto;}
    public float getRay() {return ray;}
    public void setRay(float ray) {this.ray = ray;}
    
    public Circle(){
        ponto = new Ponto();
        ray = 1;
    }
    public Circle( Ponto _ponto, float r ){
        ponto = _ponto;
        ray = r;
    }
    public float GetArea(){
        return (float) 3.14159 * ray*ray;
    }
    public float GetDiameter(){
        return ray*2;
    }   
}
