package t91;
/* author AdrianKS */

public class T91 {

    public static void main(String[] args){
        Circle c = new Circle();
        Circle c2 = new Circle( new Ponto( 5,5 ), 4 );
        
        System.out.printf( "c1 %f ", c.GetArea() );
        System.out.printf( " %f \n", c.GetDiameter() );
        
        System.out.printf( "c2 %f ", c2.GetArea() );
        System.out.printf( " %f ", c2.GetDiameter() );
    }
    
}
