package t91;
import static java.lang.Math.*;

public class Ponto {
    private float x;
    private float y;
    
    public float getX() {return x;}
    public void setX(float x) {this.x = x;}
    public float getY() {return y;}
    public void setY(float y) {this.y = y;}
    
    public Ponto(){
        x = 0;
        y = 0;
    }
    public Ponto( float _x, float _y){
        x = _x;
        y = _y;
    }
    
    public void Translate( float dx, float dy ){
        x += dx;
        y += dy;
    }
    public static float Distance( Ponto p1, Ponto p2 ){
        float dx = p1.x - p2.x;
        float dy = p1.y - p2.y;
        return (float) sqrt( dx*dx + dy*dy );
    }
}
